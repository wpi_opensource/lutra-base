# README for ansible-debian-base

This is a module to quickly setup a somewhat secure setup on Debian and Ubuntu based machines. 

## Variables 

### admin_users 

data structure containing all the shell users that should be created. Example: 

```
admin_users:
  john:
    uid: 1010
    fullname: John Doe
    ensure: present
    shell: /bin/bash
    ssh_public_key: "ssh-rsa ...."
  mike:
    uid: 1011
    fullname: Jane Doe
    ensure: present
    shell: /bin/bash
    ssh_public_key: "ssh-rsa ...."
```

### linux_root_password_hashed

hashed password to set for the root user. Remains unset / unmanaged by default

### linux_updates_mechanism

Resort to the oldskool shell script (legacy) or the native updates manager (current). Defaults to current.

###  linux_updates_mail_recipient

Mailbox to receive reports about installed updates, defaults to root

## linux_updates_automatic_reboot

Wether or not to automaticly reboot a host if any of the updates requires a restart to take effect. 
Either "true" or "false", defaults to "false" (yes quoted)

## linux_updates_reboot_time

When automatic reboots are enabled, at what time the restart is initiated. Defaults to "2:22"

### root_ssh_public_key

Public key that allows passwordless root access via SSH. Defaults to none

### system_mail_recipient 

E-mail recipient that gets all root / system mail. 

zabbix_host: "95.217.22.254"
zabbix_transport_id: "jdo-mon"
zabbix_transport_key: "dfc63cf83516d48150f7b27fe459099e4774e8be7a42d61eabd167274a92a3ac"

